package kz.astana.fileapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.preference.PreferenceManager;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sp = getSharedPreferences("My", MODE_PRIVATE);
        String name = sp.getString("KEY_NAME", "");
//        Toast.makeText(MainActivity.this, name, Toast.LENGTH_SHORT).show();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        boolean wifiState = sharedPreferences.getBoolean("wifi", false);
        Toast.makeText(MainActivity.this, wifiState + "", Toast.LENGTH_SHORT).show();

        writeToFile("myfile.txt");
        readFromFile("myfile.txt");

        if (isExternalStorageWritable()) {
            Log.d("Hello", "External storage writable");
        } else {
            Log.d("Hello", "External storage not writable");
        }

        if (isExternalStorageReadable()) {
            Log.d("Hello", "External storage readable");
        } else {
            Log.d("Hello", "External storage not readable");
        }

        ActivityCompat.requestPermissions(
                MainActivity.this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                REQUEST_CODE
        );

        Button open = findViewById(R.id.openButton);
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SharedActivity.class));
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            boolean isGranted = false;
            for (int result : grantResults) {
                if (result == PackageManager.PERMISSION_GRANTED) {
                    isGranted = true;
                } else if (result == PackageManager.PERMISSION_DENIED) {
                    isGranted = false;
                }
            }
            if (isGranted) {
                File publicDirectory = getDownloadPublicDirectory("my");
                File storageDirectory = getDownloadStorageDirectory("store");
                writeToExternalFile(storageDirectory.getAbsolutePath(), "file.txt");
                readFromExternalFile(storageDirectory.getAbsolutePath(), "file.txt");
            }
        }
    }

    private void writeToExternalFile(String dir, String filename) {
        try {
            File file = new File(dir, filename);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            PrintWriter printWriter = new PrintWriter(fileOutputStream);
            printWriter.println("Hello world!");
            printWriter.println("Vasya Pupkin!");
            printWriter.flush();
            printWriter.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFromExternalFile(String dir, String filename) {
        try {
            File file = new File(dir, filename);
            FileInputStream fileInputStream = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            String read;
            while ((read = bufferedReader.readLine()) != null) {
                stringBuffer.append(read);
            }
            Log.d("Hello", stringBuffer.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(String fileName) {
        try {
            FileOutputStream fileOutputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
            String s = "Content\nNew Content\nNew new content";
            fileOutputStream.write(s.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFromFile(String fileName) {
        try {
            InputStream inputStream = openFileInput(fileName);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            String read;
            while ((read = bufferedReader.readLine()) != null) {
                stringBuffer.append(read).append("\n");
            }
            inputStream.close();
            Log.d("Hello", stringBuffer.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED) || state.equals(Environment.MEDIA_MOUNTED_READ_ONLY);
    }

    private File getDownloadPublicDirectory(String name) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), name);
        if (!file.mkdirs()) {
            Log.e("Hello", "Public directory not created");
        }
        return file;
    }

    private File getDownloadStorageDirectory(String name) {
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), name);
        if (!file.mkdirs()) {
            Log.e("Hello", "Storage directory not created");
        }
        return file;
    }
}