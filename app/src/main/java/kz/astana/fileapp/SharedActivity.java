package kz.astana.fileapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SharedActivity extends AppCompatActivity {

    private final String KEY_NAME = "KEY_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);

        SharedPreferences sharedPreferences = getSharedPreferences("My", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_NAME, "Aslan");
        editor.commit();

        String name = sharedPreferences.getString(KEY_NAME, "Ruslan");
        Toast.makeText(SharedActivity.this, name, Toast.LENGTH_SHORT).show();

        Button open = findViewById(R.id.open);
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SharedActivity.this, SettingsActivity.class));
            }
        });
    }
}